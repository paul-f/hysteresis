function PWTT_PAP_Apnoe = apnoe(PIGnumber, lpFiltAIR, lpFiltPAP, modPAP)

switch PIGnumber
    case 12; PIGname= 'deltaPWTT_20210315_PIG12_apnoea_1.mat';
    case 13; PIGname= 'deltaPWTT_20210315_PIG13_Apnea_2.mat';
    case 14; PIGname= 'deltaPWTT_20210331_PIG14_Apnoe_1.mat';
    case 15; PIGname= 'deltaPWTT_20210412_PIG15_Apnoea_1.mat';
    case 16; PIGname= 'deltaPWTT_20210414_PIG16_Apnoea_1.mat';
end
PIG = load(['C:\Users\Paul Frenkel\Documents\Hysterese (Fabian)\Messdaten\PIG', num2str(PIGnumber), '\' ,PIGname]);
% end
PIGval= initPIG(PIGname);
fs =    1000;


PROXdistDIST = 99.8;                            % Abstand proximaler zu distaler Katheter im Schwein

switch PIGval.fs
    case 10000
        rawPAPprox  = resample(PIG.data(PIG.datastart(5):PIG.dataend(5)),1,10);    % proximal pulmonary artery pressure
        rawPAPnass  = resample(PIG.data(PIG.datastart(7):PIG.dataend(7)),1,10);
        rawPAPdist  = resample(PIG.data(PIG.datastart(8):PIG.dataend(8)),1,10);    % distal pulmonary artery pressure
        rawPairway  = resample(PIG.data(PIG.datastart(14):PIG.dataend(14)),1,10);   % airway pressure
        rawPeso     = resample(PIG.data(PIG.datastart(4):PIG.dataend(4)),1,10);     %
        rawTPP      = rawPairway - rawPeso;
        %  Dist und Proximaler Millar Katheter getauscht bei Schwein 13 ab 12:24.
        if PIGname == "deltaPWTT_20210329_PIG13_Hyst_1.mat" || PIGname == "deltaPWTT_20210329_PIG13_Hyst_3.mat" || PIGname == "deltaPWTT_20210315_PIG13_Apnea_2.mat"
            rawPAPprox_ = rawPAPdist;
            rawPAPdist = rawPAPprox;
            rawPAPprox = rawPAPprox_; clear rawPAPprox_
        elseif PIGname == "deltaPWTT_20210412_PIG15_hysterese_4.mat" || PIGname == "deltaPWTT_20210412_PIG15_hysterese_5.mat"
            clear rawPAPnass;   rawPAPnass = PIG.data(PIG.datastart(7):PIG.dataend(7));
            clear rawPairway;   rawPairway = PIG.data(PIG.datastart(14):PIG.dataend(14));   % airway pressure
            clear rawPeso;      rawPeso    = PIG.data(PIG.datastart(4):PIG.dataend(4));     %
            clear rawTPP;       rawTPP      = rawPairway - rawPeso;
        end
        PIGval.STARTanalyse = PIGval.STARTanalyse/10;
        PIGval.ENDanalyse = PIGval.ENDanalyse/10;

    case 1000               % UPSAMPLING
        rawPAPprox  = PIG.data(PIG.datastart(5):PIG.dataend(5));    % proximal pulmonary artery pressure
        rawPAPnass  = PIG.data(PIG.datastart(7):PIG.dataend(7));
        rawPAPdist  = PIG.data(PIG.datastart(8):PIG.dataend(8));    % distal pulmonary artery pressure
        rawPairway  = PIG.data(PIG.datastart(14):PIG.dataend(14));   % airway pressure
        rawPeso     = PIG.data(PIG.datastart(4):PIG.dataend(4));     %
        rawTPP      = rawPairway - rawPeso;                             %
end

% figure; hold on
% plot(rawPAPprox)
% plot(rawPAPdist)
% plot(rawPAPnass)
% plot(rawPairway)
% plot(rawPeso)
% plot(rawTPP)
% title('Gesamte geladene Datei aus Labchart')
% xlabel('N / AU'); ylabel('p / mmHg')
% legend('PAPprox', 'PAPdist', 'PAPnass', 'Pairway', 'Peso', 'TPP');


cutPAPprox  = rawPAPprox(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPAPnass  = rawPAPnass(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPAPdist  = rawPAPdist(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPairway  = rawPairway(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPeso     = rawPeso(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutTPP      = rawTPP(PIGval.STARTanalyse:PIGval.ENDanalyse);
time        = (0:length(cutPAPprox)-1)/fs*1000;

% figure; hold on
% plot(time,cutPAPprox)
% plot(time,cutPAPdist)
% plot(time,cutPAPnass)
% plot(time,cutPairway)
% plot(time,cutPeso)
% plot(time,cutTPP)
% title('Teilsignale geladene Datei aus Labchart')
% xlabel('t / ms'); ylabel('p / mmHg')
% legend('PAPprox', 'PAPdist', 'PAPnass', 'Pairway', 'Peso', 'TPP');
% % xlim([0 13500])


offset      = [0 0];                                                    % Korrektur des Offsets durch unterschiedliche Millarkatheter Kallibration und Drift.
PAPproxOffset   = cutPAPprox + offset(1);
PAPdistOffset   = cutPAPdist + offset(2);
PAPprox         = filtfilt(lpFiltPAP, PAPproxOffset);
PAPdist         = filtfilt(lpFiltPAP, PAPdistOffset);
PAPnass         = cutPAPnass;       % PAPnass= filtfilt(lpFiltPAPnass, cutPAPnass);
Pairway         = filtfilt(lpFiltAIR, cutPairway);
Peso            = filtfilt(lpFiltAIR ,cutPeso);
PAPprox= PAPprox-mean(PAPprox); PAPprox= PAPprox/max(PAPprox);
PAPdist= PAPdist-mean(PAPdist); PAPdist= PAPdist/max(PAPdist);
Pairway= Pairway-mean(Pairway); Pairway= Pairway/max(Pairway);
if offset(:) == 0; clear offset PAPdistOffset PAPproxOffset; end

[~,PAPproxMax] = findpeaks(PAPprox/max(PAPprox),  'MinPeakDistance', 400, 'Annotate','extents');
[~,PAPproxMin] = findpeaks(-PAPprox/max(-PAPprox),'MinPeakDistance', 400, 'Annotate','extents');
% PAPdist_ = PAPdist;     % die ersten Werte bis PAPproxMin(1) maximal werden lassen, damit minPeaks erst danach gefunden werden
[~,PAPdistMax] = findpeaks(PAPdist/max(PAPdist),  'MinPeakDistance', 400, 'Annotate','extents');
[~,PAPdistMin] = findpeaks(-PAPdist/max(-PAPdist),'MinPeakDistance', 400, 'Annotate','extents');
% figure; hold on;
% findpeaks(PAPprox/max(PAPprox),  'MinPeakDistance', 400, 'Annotate','extents');
% findpeaks(-PAPprox/max(-PAPprox),'MinPeakDistance', 400, 'Annotate','extents');


dPairway = gradient(Pairway,time);
d2Pairway = gradient(dPairway,time);
[~,d2PairwayMax]=   findpeaks(d2Pairway/max(d2Pairway), 'MinPeakProminence',0.5,  'Annotate','extents');

%%
% Prüfung: PAP-Paar vor Start der Beatmung finden
fristPAP= PAPproxMin(find(PAPproxMin<d2PairwayMax(1), 1,'last'));
PAPproxMin(find(PAPproxMin<fristPAP)) = [];
PAPdistMin(find(PAPdistMin<fristPAP)) = [];
PAPproxMax(find(PAPproxMax<fristPAP)) = [];

lastPAP= max([PAPdistMax(find(PAPdistMax<d2PairwayMax(end), 1,'last')), ...
    PAPproxMax(find(PAPproxMax<d2PairwayMax(end), 1,'last'))]);
PAPproxMin(find(PAPproxMin>lastPAP)) = [];
PAPdistMin(find(PAPdistMin>lastPAP)) = [];
PAPproxMax(find(PAPproxMax>lastPAP)) = [];
PAPdistMax(find(PAPdistMax>lastPAP)) = [];


% Prüfung: auf ein Minimum folgt ein Maximum
k = 1;
while k < max([length(PAPproxMin) length(PAPproxMax)])
    if PAPproxMax(k) < PAPproxMin(k)
        PAPproxMax(k)=  [];
        k = 1;
    elseif PAPproxMin(k) < PAPproxMax(k) && PAPproxMin(k+1) < PAPproxMax(k)
        PAPproxMin(k)= [];
        k = 1;
    else
        k = k+1;
    end
end; clear k
k = 1;
while k < max([length(PAPdistMin) length(PAPdistMax)])
    if PAPdistMax(k) < PAPdistMin(k)
        PAPdistMax(k)=  [];
        k = 1;
    elseif PAPdistMin(k) < PAPdistMax(k) && PAPdistMin(k+1) < PAPdistMax(k)
        PAPdistMin(k)= [];
        k = 1;
    else
        k = k+1;
    end
end; clear k

%%
% Tangenten-/TANH-Methode
coeffprox = zeros(4,length(PAPproxMax)-6);
coeffdist = zeros(4,length(PAPdistMax)-6);

MODEL=  @(coeff,timeSeg) coeff(1)*tanh((timeSeg-coeff(2))/coeff(3))+coeff(4);
        for pos= 4:length(PAPproxMax)-3
            coeffprox(:,pos-3)=   estTANH(time, PAPprox, PAPproxMin(pos), PAPproxMax(pos));
            coeffdist(:,pos-3)=   estTANH(time, PAPdist, PAPdistMin(pos), PAPdistMax(pos));
        end; clear pos
        PWTT = coeffdist(2,:) - coeffprox(2,:);
        PWTT(PWTT==0) = nan;


PWTT_PAP = zeros(5,length(PWTT)-1);                                                 % PWTT_PAP = [PWTT, PWTTstart, PAP, PAPloc, locCycle]
PWTT_PAP(1,:) = PWTT(1:end-1);
PWTT_PAP(2,:) = PAPproxMin(1,1:length(PWTT)-1);

switch modPAP
    case 'sPAP'
        PWTT_PAP_Apnoe = pwtt_pap(PWTT_PAP, PAPnass, PAPproxMin(1,:), d2PairwayMax, time, 'sPAP');
    case 'dPAP'
        PWTT_PAP_Apnoe = pwtt_pap(PWTT_PAP, PAPnass, PAPproxMin(1,:), d2PairwayMax, time, 'dPAP');
    case 'mPAP'
        PWTT_PAP_Apnoe = pwtt_pap(PWTT_PAP, PAPnass, PAPproxMin(1,:), d2PairwayMax, time, 'mPAP');
end
end