function [data] = initPIG(name)

fs = 10000;
switch name
    % PIG 12
    case 'deltaPWTT_20210315_PIG12_hysterese_1.mat'
        fs = 1000;
        STARTanalyse= 94000;                                % so wählen, dass das proximale Signale kurz vor seinem lok. Min. beginnt
        ENDanalyse  = 347500;                               % so wählen, dass das distale Signale kurz nach seinem lok. Max. endet
    case 'deltaPWTT_20210315_PIG12_hysterese_2.mat'
        fs = 1000;
        STARTanalyse = 2050;
        ENDanalyse = 225860;
    case 'deltaPWTT_20210315_PIG12_hysterese_21.mat'
        fs = 1000;
        STARTanalyse = 1400;
        ENDanalyse = 47450;
    case 'deltaPWTT_20210315_PIG12_hysterese_22.mat'
        fs = 1000;
        STARTanalyse = 4150;
        ENDanalyse = 94950;
    case 'deltaPWTT_20210315_PIG12_apnoea_1.mat'
        fs = 1000;
        STARTanalyse = 12500;
        ENDanalyse = 42000;
        %     case 'deltaPWTT_20210315_PIG12_hysterese_24.mat'
        %         fs = 1000;
        %         STARTanalyse = 1050;
        %         ENDanalyse = 225560;

        % PIG13
    case 'deltaPWTT_20210329_PIG13_Hyst_1.mat'
        STARTanalyse= 700000;                     % 716600
        ENDanalyse  = 3250000;
    case 'deltaPWTT_20210329_PIG13_Hyst_3.mat'
        STARTanalyse= 39000;
        ENDanalyse  = 1950000;
    case 'deltaPWTT_20210315_PIG13_Apnea_2.mat'
        STARTanalyse = 210000;
        ENDanalyse = 620000;

        % PIG14
    case 'deltaPWTT_20210331_PIG14_hyst_1.mat'
        STARTanalyse= 2500000;
        ENDanalyse  = 5030000;
    case 'deltaPWTT_20210331_PIG14_Apnoe_1.mat'
        STARTanalyse= 150000;
        ENDanalyse  = 602000;

        % PIG15
    case 'deltaPWTT_20210412_PIG15_hysterese_1.mat'
        STARTanalyse= 1000;
        ENDanalyse  = 2960000;
    case 'deltaPWTT_20210412_PIG15_hysterese_2.mat'
        STARTanalyse= 23000;
        ENDanalyse  = 1635000;
    case 'deltaPWTT_20210412_PIG15_hysterese_3.mat'
        STARTanalyse= 20000;
        ENDanalyse  = 2540000;
    case 'deltaPWTT_20210412_PIG15_hysterese_4.mat'
        STARTanalyse= 1540;
        ENDanalyse  = 1025550;
    case 'deltaPWTT_20210412_PIG15_hysterese_5.mat'
        STARTanalyse= 1450;
        ENDanalyse  = 1490500;
    case 'deltaPWTT_20210412_PIG15_Apnoea_1.mat'
        STARTanalyse = 157000;
        ENDanalyse = 500000;

        % PIG 16
    case 'deltaPWTT_20210414_PIG16_hyst_1.mat'
        STARTanalyse= 5000;
        ENDanalyse  = 2525000;
    case 'deltaPWTT_20210414_PIG16_Apnoea_1.mat'
        STARTanalyse = 200000;
        ENDanalyse = 630000;
end

data.STARTanalyse = STARTanalyse;
data.ENDanalyse = ENDanalyse;
data.fs = fs;
end