%% init
clear
close all
addpath(genpath('C:\Users\Paul Frenkel\Documents\Datenaustausch\hysteresis'))

%% ENTER PIGNUMBER 12 - 16 !!!
PIGnumber=  12;


%%
[PAPprox, PAPdist, PAPnass, Pairway, Peso, TPP, TVPeso, TVPaw, lpFiltAIR, lpFiltPAP]= loadPIG( PIGnumber );

fs=         1000;
time=       (0:length(PAPprox)-1)/fs*1000;


%% finding minima and maxima of PAPprox/PAPdist
[~,PAPproxMax]= findpeaks( PAPprox/max(PAPprox),  'MinPeakDistance', 400, 'Annotate','extents' );
[~,PAPproxMin]= findpeaks( -PAPprox/max(-PAPprox),'MinPeakDistance', 400, 'Annotate','extents' );
[~,PAPdistMax]= findpeaks( PAPdist/max(PAPdist),  'MinPeakDistance', 400, 'Annotate','extents');
[~,PAPdistMin]= findpeaks( -PAPdist/max(-PAPdist),'MinPeakDistance', 400, 'Annotate','extents' );

dPairway=               gradient(Pairway,time);
dPairway( dPairway<0 )= 0;
d2Pairway=              gradient(filtfilt(lpFiltAIR,dPairway),time);
[~,d2PairwayMax]=       findpeaks(d2Pairway/max(d2Pairway), 'MinPeakProminence',0.8,  'Annotate','extents');

% Check: PAP-Pair within a Resp-Cycle?
fristPAP= PAPproxMin( find(PAPproxMin<d2PairwayMax(1), 1,'last') );
PAPproxMin(PAPproxMin<fristPAP)=  [];
PAPdistMin(PAPdistMin<fristPAP)=  [];
PAPproxMax(PAPproxMax<fristPAP)=  [];

lastPAP= max([PAPdistMax(find(PAPdistMax<d2PairwayMax(end), 1,'last')), ...
    PAPproxMax(find(PAPproxMax<d2PairwayMax(end), 1,'last'))]);
PAPproxMin(PAPproxMin>lastPAP)=   [];
PAPdistMin(PAPdistMin>lastPAP)=   [];
PAPproxMax(PAPproxMax>lastPAP)=   [];
PAPdistMax(PAPdistMax>lastPAP)=   [];

% Check: minima follow maxima
k = 1;
while k < max([length(PAPproxMin) length(PAPproxMax)])
    if PAPproxMax(k) < PAPproxMin(k)
        PAPproxMax(k)=  [];
        k = 1;
    elseif PAPproxMin(k) < PAPproxMax(k) && PAPproxMin(k+1) < PAPproxMax(k)
        PAPproxMin(k)= [];
        k = 1;
    else
        k = k+1;
    end
end; clear k
k = 1;
while k < max([length(PAPdistMin) length(PAPdistMax)])
    if PAPdistMax(k) < PAPdistMin(k)
        PAPdistMax(k)=  [];
        k = 1;
    elseif PAPdistMin(k) < PAPdistMax(k) && PAPdistMin(k+1) < PAPdistMax(k)
        PAPdistMin(k)= [];
        k = 1;
    else
        k = k+1;
    end
end; clear k

% % control plot
% i = length(PAPproxMax);
% figure; hold on
% plot(PAPprox); plot(PAPdist)
% plot(PAPproxMin(i), PAPprox(PAPproxMin(i)),'+'); plot(PAPproxMax(i), PAPprox(PAPproxMax(i)),'+')
% plot(PAPdistMin(i), PAPdist(PAPdistMin(i)),'+'); plot(PAPdistMax(i), PAPdist(PAPdistMax(i)),'+')
% xlim([PAPproxMin(i)-500 PAPproxMax(i)+500])

%% estimation of PWTT
coeffprox = zeros(4,length(PAPproxMax));
coeffdist = zeros(4,length(PAPdistMax));

MODEL=  @(coeff,timeSeg) coeff(1)*tanh((timeSeg-coeff(2))/coeff(3))+coeff(4);
for pos= 1:length(PAPproxMax)
    coeffprox(:,pos)=   estTANH(time, PAPprox, PAPproxMin(pos), PAPproxMax(pos));
    coeffdist(:,pos)=   estTANH(time, PAPdist, PAPdistMin(pos), PAPdistMax(pos));
end; clear pos
PWTT = coeffdist(2,:) - coeffprox(2,:);
PWTT(PWTT==0) = nan;


% % control plot
% fac = 1;
% coeffprox(2,:) = coeffprox(2,:)-fac;
% coeffdist(2,:) = coeffdist(2,:)-fac;
% 
% figure; hold on
% plot(time,PAPprox)
% plot(time,PAPdist)
% for pos = 1:5
%     time_ = time(PAPproxMin(pos)-20:PAPproxMax(pos)+20);
%     plot(time_,MODEL(coeffprox(:,pos),time_),'--', 'Color',[0.4660, 0.6740, 0.1880])
%     plot(time_,MODEL(coeffdist(:,pos),time_),'--','Color',[0.3010, 0.7450, 0.9330])
% end; clear pos
% xlim([0 2000]);% ylim([min(PAPprox) max(PAPdist)])



%% times within Resp-Cycle (color of points in plot)
dPairway = gradient(Pairway,time);
[~,dPairwayMin] = findpeaks(-dPairway/max(dPairway), 'MinPeakProminence',0.8,  'Annotate','extents');
dPairwayNeg = find(dPairway<0); dPairway(dPairwayNeg) = 0;
dPairwayFilt = filtfilt(lpFiltAIR,dPairway);
d2Pairway = gradient(dPairwayFilt,time);
[~,d2PairwayMax] = findpeaks(d2Pairway/max(d2Pairway), 'MinPeakProminence',0.8,  'Annotate','extents');
dPairway = gradient(Pairway,time);
dPairwayPos = find(dPairway>0); dPairway(dPairwayPos) = 0;
dPairwayFilt = filtfilt(lpFiltAIR,dPairway);
d2Pairway = gradient(dPairwayFilt,time);
[~,d2PairwayMin] = findpeaks(d2Pairway/max(d2Pairway), 'MinPeakProminence',0.8,  'Annotate','extents');

% % control plot
% figure
% subplot(2,1,1); hold on
% plot(Pairway)
% plot(d2PairwayMax,Pairway(d2PairwayMax),'+', 'Color',[0, 0.4470, 0.7410])
% xlim([3000 14000])
% subplot(2,1,2); hold on
% plot(Pairway-2)
% plot(PAPprox)
% plot(PAPdist)
% xlim([3000 14000])


%% sPAP (systolisch), dPAP (diastolisch) und mPAP (durchschnittl.) aus PAPnass
PWTT_PAP = zeros(5,length(PWTT)-1);                                                 % PWTT_PAP = [PWTT, PWTTstart, PAP, PAPloc, locCycle]
PWTT_PAP(1,:) = PWTT(1:end-1);
PWTT_PAP(2,:) = PAPproxMin(1,1:length(PWTT)-1);

PWTT_sPAP=  pwtt_pap(PWTT_PAP, PAPnass, PAPproxMin(1,:), d2PairwayMax, time, 'sPAP');
PWTT_dPAP=  pwtt_pap(PWTT_PAP, PAPnass, PAPproxMin(1,:), d2PairwayMax, time, 'dPAP');
PWTT_mPAP=  pwtt_pap(PWTT_PAP, PAPnass, PAPproxMin(1,:), d2PairwayMax, time, 'mPAP');
PWTT_TVPeso=pwtt_pap(PWTT_PAP, TVPeso, PAPproxMin(1,:), d2PairwayMax, time, 'mPAP');
PWTT_TVPaw= pwtt_pap(PWTT_PAP, TVPaw, PAPproxMin(1,:), d2PairwayMax, time, 'mPAP');

% apnoe
PWTT_sPAP_Apnoe = apnoe(PIGnumber, lpFiltAIR, lpFiltPAP, 'sPAP');
PWTT_dPAP_Apnoe = apnoe(PIGnumber, lpFiltAIR, lpFiltPAP, 'dPAP');
PWTT_mPAP_Apnoe = apnoe(PIGnumber, lpFiltAIR, lpFiltPAP, 'mPAP');

%
k3 = 0;
for k1 = 1:length(d2PairwayMax)-1
    k2 = find(dPairwayMin>d2PairwayMax(k1),1,'first');
    if isempty(k2) == 1
        break
    end
    k3= k3+1;
    startIn=    find(PWTT_PAP(2,:)>=d2PairwayMax(k1),1,'first');
    endIn=      find(PWTT_PAP(2,:)<dPairwayMin(k2),1,'last');
    endEx=      find(PWTT_PAP(2,:)<d2PairwayMax(k1+1),1,'last');
    meanResp(1,k3)=     mean(PWTT_PAP(1,startIn:endIn));
    meanResp(2,k3)=     mean(PWTT_PAP(1,endIn+1:endEx));
    meanResp(3,k3)=     round(mean([d2PairwayMax(k1),dPairwayMin(k2)]));
    meanResp(4,k3)=     round(mean([dPairwayMin(k2),d2PairwayMax(k1+1)]));
    clear startIn endIn endEx
end; clear k1 k2 k3

% airway
PWTT_Peso = PWTT_PAP(:,1:end-1); %PWTT_Peso(4:5,:)=[];
for i = 1:length(PWTT_Peso)
    PWTT_Peso(3,i)= mean(Peso(PWTT_PAP(2,i):PWTT_PAP(2,i+1)));
end
PWTT_TPP = PWTT_PAP(:,1:end-1);% PWTT_TPP(4:5,:)=[];
for i = 1:length(PWTT_TPP)
    PWTT_TPP(3,i)= mean(TPP(PWTT_PAP(2,i):PWTT_PAP(2,i+1)));
end
PWTT_Pairway = PWTT_PAP(:,1:end-1); %PWTT_Pairway(4:5,:)=[];
for i = 1:length(PWTT_Pairway)
    PWTT_Pairway(3,i)= mean(Pairway(PWTT_PAP(2,i):PWTT_PAP(2,i+1)));
    PWTT_Pairway(4,i)= PWTT_mPAP(3,i);
end





%% Plots
% TANH-method
gcf= figure; hold on
set(gcf, 'Units','normalized', 'position',[0.3628 0.5460 0.2734*1.5 0.3646]);
set(gca, 'FontName', 'Palatino Linotype', 'FontSize', 10*10/7)
plot(time,PAPprox, 'LineWidth',.75)
plot(time,PAPdist, 'LineWidth',.75)
for pos = 5:6
    time_ = time(PAPproxMin(1,pos)-150:PAPproxMax(1,pos)+150);
    plot(time_,MODEL(coeffprox(:,pos),time_),'--', 'Color',[0, 0.4470, 0.7410],      'LineWidth',1.2)
    plot(time_,MODEL(coeffdist(:,pos),time_),'--', 'Color',[0.8500, 0.3250, 0.0980], 'LineWidth',1.2)
    xline(coeffprox(2,pos),':')
    xline(coeffdist(2,pos),':')
    plot(coeffprox(2,pos),coeffprox(4,pos),'.', 'Color',[0, 0.4470, 0.7410],      'MarkerSize',20)
    plot(coeffdist(2,pos),coeffdist(4,pos),'.', 'Color',[0.8500, 0.3250, 0.0980], 'MarkerSize',20)
    clear time_
end; clear pos
xlim([PAPproxMin(1,5)-250 PAPproxMax(1,6)+250]);
xlabel('Time [ms]')
ylabel('PAP [AU]')



% colorbar
figure
set(gca,'Visible',false)
set(gca, 'FontName', 'Palatino Linotype')
cbar=colorbar;
colormap turbo
caxis([0 5]);
xlabel(cbar,'time / s')



% PWTT(PAP) with apnoe
for k = 2:length(dPairwayMin)       % PWTT wird erst ab Zeitpunkt des 1. Atemzyklus berechnet, darum liegen vorher keine Werte vor
    inspTime(k-1)= d2PairwayMin(k)-d2PairwayMax(k);
end; clear endIn endEx k
inspTime= mean(inspTime);
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_sPAP(3,PWTT_mPAP(5,:)<inspTime),PWTT_sPAP(1,PWTT_mPAP(5,:)<inspTime), 25, PWTT_sPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_sPAP(3,PWTT_mPAP(5,:)>=inspTime),PWTT_sPAP(1,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_sPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
scatter (PWTT_sPAP_Apnoe(3,:),PWTT_sPAP_Apnoe(1,:), 25, 'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[1 1 1], 'LineWidth',1.5)
axis square
xlabel('sPAP [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter (PWTT_mPAP(3,PWTT_mPAP(5,:)<inspTime),PWTT_mPAP(1,PWTT_mPAP(5,:)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_mPAP(3,PWTT_mPAP(5,:)>=inspTime),PWTT_mPAP(1,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
scatter (PWTT_mPAP_Apnoe(3,:),PWTT_mPAP_Apnoe(1,:), 25, 'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[1 1 1], 'LineWidth',1.5)
axis square
xlabel('mPAP [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,3); hold on
scatter (PWTT_dPAP(3,PWTT_mPAP(5,:)<inspTime),PWTT_dPAP(1,PWTT_mPAP(5,:)<inspTime), 25, PWTT_dPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_dPAP(3,PWTT_mPAP(5,:)>=inspTime),PWTT_dPAP(1,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_dPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
scatter (PWTT_dPAP_Apnoe(3,:),PWTT_dPAP_Apnoe(1,:), 25, 'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[1 1 1], 'LineWidth',1.5)
axis square
xlabel('dPAP [mmHg]'); ylabel('PWTT [ms]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% PWTT(mPAP) -> respiratory gating
for k = 2:length(dPairwayMin)       % PWTT wird erst ab Zeitpunkt des 1. Atemzyklus berechnet, darum liegen vorher keine Werte vor
    endIn=      find( PWTT_dPAP(4,:) <= dPairwayMin(k), 1,'last' );         % letzter dPAP zur Bestimmung des letzten, vollständigen Herzschlags innerhalb der Insp
    endEx=      find( PWTT_dPAP(4,:) <= d2PairwayMax(k), 1,'last' );        % letzter dPAP zur Bestimmung des letzten, vollständigen Herzschlags innerhalb der Exsp
    endIn_sPAP(:,k-1)= PWTT_sPAP(:,endIn-1);
    endEx_sPAP(:,k-1)= PWTT_sPAP(:,endEx-1);
    endIn_dPAP(:,k-1)= PWTT_dPAP(:,endIn-1);
    endEx_dPAP(:,k-1)= PWTT_dPAP(:,endEx-1);
    endIn_mPAP(:,k-1)= PWTT_mPAP(:,endIn-1);
    endEx_mPAP(:,k-1)= PWTT_mPAP(:,endEx-1);
end; clear endIn endEx k
figure; hold on
subplot(1,3,2); hold on
scatter (PWTT_mPAP(3,:),PWTT_mPAP(1,:), 25, 'filled', 'MarkerFaceColor', [.8 .8 .8])
scatter (endIn_mPAP(3,:),endIn_mPAP(1,:), 25, 'filled', 'MarkerFaceColor', [0.1010 0.4750 0.9330])
scatter (endEx_mPAP(3,:),endEx_mPAP(1,:), 25, 'filled', 'MarkerFaceColor', [0.6350 0.0780 0.1840])
axis square
xlabel('mPAP [mmHg]'); ylabel('PWTT [ms]')
set(gca,'FontName','Palatino Linotype');



% PWTT(Pair)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_Pairway(1,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_Pairway(1,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{aw} [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_Peso(1,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_Peso(1,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{eso} [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,3); hold on
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_TPP(1,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_TPP(1,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('TPP [mmHg]'); ylabel('PWTT [ms]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% mPAP(Pair)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_Pairway(4,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_Pairway(4,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{aw} [mmHg]'); ylabel('mPAP [mmHg]')
subplot(1,3,2); hold on
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{eso} [mmHg]'); ylabel('mPAP [mmHg]')
subplot(1,3,3); hold on
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('TPP [mmHg]'); ylabel('mPAP [mmHg]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% PWTT(Paw) / mPAP(Paw)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_Pairway(1,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_Pairway(1,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{aw} [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_Pairway(4,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Pairway(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_Pairway(4,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{aw} [mmHg]');  ylabel('mPAP [mmHg]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% PWTT(Peso) / mPAP(Peso)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_Peso(1,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_Peso(1,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{eso} [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_Peso(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('P_{eso} [mmHg]'); ylabel('mPAP [mmHg]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% PWTT(TPP) / mPAP(TPP)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_TPP(1,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_TPP(1,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('TPP [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)<inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TPP(3,PWTT_mPAP(5,1:end-1)>=inspTime),PWTT_mPAP(3,PWTT_mPAP(5,1:end-1)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,1:end-1)>=inspTime)/1000, 'filled')
axis square
xlabel('TPP [mmHg]');  ylabel('mPAP [mmHg]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% PWTT(TVPeso) / mPAP(TVPeso)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_TVPeso(3,PWTT_mPAP(5,:)<inspTime),PWTT_TVPeso(1,PWTT_mPAP(5,:)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TVPeso(3,PWTT_mPAP(5,:)>=inspTime),PWTT_TVPeso(1,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
axis square
xlabel('TVP_{eso} [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter (PWTT_TVPeso(3,PWTT_mPAP(5,:)<inspTime),PWTT_mPAP(3,PWTT_mPAP(5,:)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TVPeso(3,PWTT_mPAP(5,:)>=inspTime),PWTT_mPAP(3,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
axis square
xlabel('TVP_{eso} [mmHg]');  ylabel('mPAP [mmHg]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');



% PWTT(TVPaw) / mPAP(TVPaw)
gcf=figure;
subplot(1,3,1); hold on
scatter (PWTT_TVPaw(3,PWTT_mPAP(5,:)<inspTime),PWTT_TVPaw(1,PWTT_mPAP(5,:)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter (PWTT_TVPaw(3,PWTT_mPAP(5,:)>=inspTime),PWTT_TVPaw(1,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
axis square
xlabel('TVP_{aw} [mmHg]'); ylabel('PWTT [ms]')
subplot(1,3,2); hold on
scatter(PWTT_TVPaw(3,PWTT_mPAP(5,:)<inspTime),PWTT_mPAP(3,PWTT_mPAP(5,:)<inspTime), 25, PWTT_mPAP(5,PWTT_mPAP(5,:)<inspTime)/1000, 'x', 'LineWidth', 1.5)
scatter(PWTT_TVPaw(3,PWTT_mPAP(5,:)>=inspTime),PWTT_mPAP(3,PWTT_mPAP(5,:)>=inspTime), 20, PWTT_mPAP(5,PWTT_mPAP(5,:)>=inspTime)/1000, 'filled')
axis square
xlabel('TVP_{aw} [mmHg]');  ylabel('mPAP [mmHg]')
colormap jet;   caxis([0 5]);
set(findobj(gcf,'type','axes'),'FontName','Palatino Linotype');

