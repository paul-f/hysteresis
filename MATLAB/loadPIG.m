function [PAPprox, PAPdist, PAPnass, Pairway, Peso, TPP, TVPeso, TVPaw, lpFiltAIR, lpFiltPAP]= loadPIG(PIGnumber)

switch PIGnumber
    case 12; PIGname=   'deltaPWTT_20210315_PIG12_hysterese_1.mat';
    case 13; PIGname=   'deltaPWTT_20210329_PIG13_Hyst_1.mat';
    case 14; PIGname=   'deltaPWTT_20210331_PIG14_hyst_1.mat';
    case 15; PIGname=   'deltaPWTT_20210412_PIG15_hysterese_3.mat';
    case 16; PIGname=   'deltaPWTT_20210414_PIG16_hyst_1.mat';
end

PIG=        load(['C:\Users\Paul Frenkel\Documents\Hysterese (Fabian)\Messdaten\PIG', num2str(PIGnumber), '\' ,PIGname]);
PIGval=     initPIG(PIGname);
fs =        1000;


%% Auswertung der Kurve von-bis in ms
% --- Chanel Setting LabChart: ---
% 1 - Pulsoxymeter
% 2 - Kardiomikrophon
% 3 - Fingerpulssensor
% 4 - Peso
% 5 - Millar1 - prox.#
% 6 - QBA2-ZVD nass
% 7 - QBA3-PAP nass (analoger Katheter mit Wasserseule - distal)
% 8 - Millar2 - dist.
% 9 - P Aorta nass
% 10- P A. fem. PICCO
% 11- ECG
% 12- EIT-Sync-in
% 13-
% 14- P Airway sync

switch PIGval.fs
    case 10000
        rawPAPprox= resample(PIG.data(PIG.datastart(5):PIG.dataend(5)),1,10);    % proximal pulmonary artery pressure
        rawPAPnass= resample(PIG.data(PIG.datastart(7):PIG.dataend(7)),1,10);
        rawPAPdist= resample(PIG.data(PIG.datastart(8):PIG.dataend(8)),1,10);    % distal pulmonary artery pressure
        rawPairway= resample(PIG.data(PIG.datastart(14):PIG.dataend(14)),1,10);   % airway pressure
        rawPeso=    resample(PIG.data(PIG.datastart(4):PIG.dataend(4)),1,10);     %
        rawTPP=     rawPairway - rawPeso;                         %
        rawTVPeso=  rawPAPnass - rawPeso;
        rawTVPaw=   rawPAPnass - rawPairway;
        %  Dist und Proximaler Millar Katheter getauscht bei Schwein 13 ab 12:24.
        if PIGname == "deltaPWTT_20210329_PIG13_Hyst_1.mat" || PIGname == "deltaPWTT_20210329_PIG13_Hyst_3.mat"
            rawPAPprox_ = rawPAPdist;
            rawPAPdist = rawPAPprox;
            rawPAPprox = rawPAPprox_; clear rawPAPprox_
        elseif PIGname == "deltaPWTT_20210412_PIG15_hysterese_4.mat" || PIGname == "deltaPWTT_20210412_PIG15_hysterese_5.mat"
            clear rawPAPnass;   rawPAPnass= PIG.data(PIG.datastart(7):PIG.dataend(7));
            clear rawPairway;   rawPairway= PIG.data(PIG.datastart(14):PIG.dataend(14));   % airway pressure
            clear rawPeso;      rawPeso=    PIG.data(PIG.datastart(4):PIG.dataend(4));     %
            clear rawTPP;       rawTPP=     rawPairway - rawPeso;
            clear rawTVPeso;    rawTVPeso=  rawPAPnass - rawPeso;
            clear rawTVPaw;     rawTVPaw=   rawPAPnass - rawPairway;
        end
        PIGval.STARTanalyse = PIGval.STARTanalyse/10;
        PIGval.ENDanalyse = PIGval.ENDanalyse/10;

    case 1000               % UPSAMPLING
        rawPAPprox= PIG.data(PIG.datastart(5):PIG.dataend(5));    % proximal pulmonary artery pressure
        rawPAPnass= PIG.data(PIG.datastart(7):PIG.dataend(7));
        rawPAPdist= PIG.data(PIG.datastart(8):PIG.dataend(8));    % distal pulmonary artery pressure
        rawPairway= PIG.data(PIG.datastart(14):PIG.dataend(14));  % airway pressure
        rawPeso=    PIG.data(PIG.datastart(4):PIG.dataend(4));    %
        rawTPP=     rawPairway - rawPeso;                         %
        rawTVPeso=  rawPAPnass - rawPeso;
        rawTVPaw=   rawPAPnass - rawPairway;
end

% % control plot
% figure; hold on
% plot(rawPAPprox)
% plot(rawPAPdist)
% plot(rawPAPnass)
% plot(rawPairway)
% plot(rawPeso)
% plot(rawTPP)
% title('Gesamte geladene Datei aus Labchart')
% xlabel('N / AU'); ylabel('p / mmHg')
% legend('PAPprox', 'PAPdist', 'PAPnass', 'Pairway', 'Peso', 'TPP');


%% Teilsignale
cutPAPprox  = rawPAPprox(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPAPnass  = rawPAPnass(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPAPdist  = rawPAPdist(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPairway  = rawPairway(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutPeso     = rawPeso(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutTPP      = rawTPP(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutTVPeso   = rawTVPeso(PIGval.STARTanalyse:PIGval.ENDanalyse);
cutTVaw     = rawTVPaw(PIGval.STARTanalyse:PIGval.ENDanalyse);
time        = (0:length(cutPAPprox)-1)/fs*1000;

% % control plot
% figure; hold on
% plot(time,cutPAPprox)
% plot(time,cutPAPdist)
% plot(time,cutPAPnass)
% plot(time,cutPairway)
% plot(time,cutPeso)
% plot(time,cutTPP)
% title('Teilsignale geladene Datei aus Labchart')
% xlabel('t / ms'); ylabel('p / mmHg')
% legend('PAPprox', 'PAPdist', 'PAPnass', 'Pairway', 'Peso', 'TPP');
% xlim([0 13500])



%% filter

%     % control plot
%     [Y, fAxis] = ffts((cutPAPnass-mean(cutPAPnass)).*window(@hamming,length(cutPAPnass))', length(cutPAPnass)*10, fs);
%     figure; plot(fAxis,Y); xlim([0 20])
lpFiltAIR = designfilt('lowpassfir', ...
    'FilterOrder', 100, ...
    'PassbandFrequency', 1, ...
    'StopbandFrequency', 15, ...
    'SampleRate', fs, ...
    'DesignMethod', 'equiripple');
lpFiltPAP = designfilt('lowpassiir', ...
    'FilterOrder', 3, ...
    'PassbandFrequency', 25, ...
    'PassbandRipple', 0.01, ...
    'SampleRate', fs);
% lpFiltPAPnass = designfilt('lowpassiir', ...
%     'FilterOrder', 3, ...
%     'PassbandFrequency', 4, ...
%     'PassbandRipple', 0.01, ...
%     'SampleRate', fs);
%     % control plot
%     fvtool(lpFiltPAP, Analysis="freq")
%     fvtool(lpFiltAIR, Analysis="freq")
%     fvtool(lpFiltPAPnass, Analysis="freq")

PAPprox=        filtfilt(lpFiltPAP, cutPAPprox);   PAPprox_=PAPprox;
PAPdist=        filtfilt(lpFiltPAP, cutPAPdist);   PAPdist_=PAPdist;
PAPnass=        cutPAPnass;       % PAPnass= filtfilt(lpFiltPAPnass, cutPAPnass);
Pairway=        filtfilt(lpFiltAIR, cutPairway);
Peso=           filtfilt(lpFiltPAP ,cutPeso);
TPP=            cutTPP;
TVPeso=         cutTVPeso;
TVPaw=          cutTVaw;
PAPprox= PAPprox-mean(PAPprox); PAPprox= PAPprox/max(PAPprox);
PAPdist= PAPdist-mean(PAPdist); PAPdist= PAPdist/max(PAPdist);
Pairway= Pairway-mean(Pairway); Pairway= Pairway/max(Pairway);

% % control plot
%
% figure; hold on; plot(cutPAPprox); plot(PAPprox); xlim([0 1000])
% figure; hold on; plot(cutPAPnass); plot(PAPnass); xlim([0 1000])
% figure; hold on; plot(cutPairway); plot(Pairway); xlim([0 1000])
%
% cutPAPprox_= cutPAPprox-mean(cutPAPprox); cutPAPprox_= cutPAPprox_/max(cutPAPprox_);
% cutPAPdist_= cutPAPdist-mean(cutPAPdist); cutPAPdist_= cutPAPdist_/max(cutPAPdist_);
% cutPairway_= cutPairway-mean(cutPairway); cutPairway_= cutPairway_/max(cutPairway_);
% figure; hold on
% p1=plot(time,cutPAPprox_, 'Color',[0, 0.4470, 0.7410]);
% p1.Color(4) = 0.2;
% plot(time,PAPprox, 'Color',[0, 0.4470, 0.7410]);
% p2=plot(time,cutPAPdist_, 'Color',[0.8500, 0.3250, 0.0980]);
% p2.Color(4) = 0.2;
% plot(time,PAPdist, 'Color',[0.8500, 0.3250, 0.0980]);
% p3=plot(time,cutPairway_-1.5, 'Color',[0.4660 0.6740 0.1880]);
% p3.Color(4) = 0.2;
% plot(time,Pairway-1.5, 'LineWidth',1, 'Color',[0.4660 0.6740 0.1880]);
% xlabel('t / s'); ylabel('p / mmHg')
% legend('', 'PAPprox','', 'PAPdist', '', 'Pairway');
% xlim([0 5000])
% clear p1 p2 p3
% clear cutPAPprox_ cutPAPdist_ cutPairway_

end