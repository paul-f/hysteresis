function PWTT_PAP = pwtt_pap(PWTT_PAP, PAPnass, locPAPproxMin, locd2Pairway, time, mod)

% calc PAP
switch mod
    case 'sPAP'
        for i = 1:length(PWTT_PAP)
            [PWTT_PAP(3,i),PWTT_PAP(4,i)] = max(PAPnass(locPAPproxMin(i):locPAPproxMin(i+1)));          % sPAP value
            PWTT_PAP(4,i) = PWTT_PAP(4,i)+locPAPproxMin(i)-1;                                           % sPAP location
        end; clear i
    case 'dPAP'
        for i = 1:length(PWTT_PAP)
            [PWTT_PAP(3,i),PWTT_PAP(4,i)] = min(PAPnass(locPAPproxMin(i)-100:locPAPproxMin(i)+100));    % dPAP value
            PWTT_PAP(4,i) = PWTT_PAP(4,i)+locPAPproxMin(i)-101;                                         % dPAP location
        end; clear i
    case 'mPAP'
        for i = 1:length(PWTT_PAP)
            [~,locMinStart]=min(PAPnass(locPAPproxMin(i)-100:locPAPproxMin(i)+100));
            [~,locMinEnd]=  min(PAPnass(locPAPproxMin(i+1)-100:locPAPproxMin(i+1)+100));
            locMinStart =   locMinStart - 101 + locPAPproxMin(i);
            locMinEnd =     locMinEnd - 101 + locPAPproxMin(i+1);
            PWTT_PAP(3,i) = mean(PAPnass(locMinStart:locMinEnd));                                       % mPAP value
            PWTT_PAP(4,i) = ceil((locMinStart+locMinEnd)/2);                                            % mPAP location
        end; clear i
end

% position within resp-cycles (for color of points)
for i = 1:length(locd2Pairway)-1
    for k = 1:length(PWTT_PAP(1,:))
        if PWTT_PAP(4,k) >= locd2Pairway(i) && PWTT_PAP(4,k) < locd2Pairway(i+1)                        % start - end of cycle
%             disp([i,k])
            PWTT_PAP(5,k) = time(PWTT_PAP(4,k) - locd2Pairway(i) +1);                                   % PAP location innerhalb eines Zyklus
%             disp([PWTT_PAP(4,k), locd2Pairway(i), PWTT_PAP(5,k)])
        end
    end
end; clear i k

% delete values outside of resp-cycles
for i = 1:length(PWTT_PAP(5,:))
    if PWTT_PAP(5,i) == 0
        PWTT_PAP(:,i) = nan(length(PWTT_PAP(:,i)),1); 
    end
end; clear i


end