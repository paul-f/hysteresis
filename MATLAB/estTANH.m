function coeff = estTANH(x, y, yMinLoc, yMaxLoc)

min2max=    round(length(yMinLoc:yMaxLoc)*2);

ySeg_=      y(yMinLoc-min2max:yMaxLoc+min2max);
ySeg_Mean=  mean(ySeg_);   ySeg_ = ySeg_- ySeg_Mean;
ySeg=       ySeg_.*window(@hamming,length(ySeg_))';
xSeg=       x(yMinLoc-min2max:yMaxLoc+min2max) - x(yMinLoc);
% figure; hold on; plot(ySeg_); plot(window(@hamming,length(ySeg_))'); plot(ySeg)
[minSegVal, minSegLoc] =    min(ySeg);
[maxSegVal, maxSegLoc] =    max(ySeg); 
% cftool(xSeg,ySeg_)
% cftool(xSeg,ySeg)
% cftool(xSeg,ySeg_minmax)

ySeg_minmax =               ySeg;
ySeg_minmax(1:minSegLoc)=   minSegVal;
ySeg_minmax(maxSegLoc:end)= maxSegVal;


% ySeg0=  zeros(1,5*length(yMinLoc:yMaxLoc));
% ySeg0(2*length(yMinLoc:yMaxLoc):2*length(yMinLoc:yMaxLoc)+length(yMinLoc:yMaxLoc)-1)=  y(yMinLoc:yMaxLoc)-y(yMinLoc);
% ySegW=  ySeg0.*window(@hamming,length(ySeg0))';
% ySeg= ySegW;
% ySeg(2*length(yMinLoc:yMaxLoc)+length(yMinLoc:yMaxLoc)-1:end)= max(ySeg);
% parametric model
MODEL=  @(coeff,x) coeff(1)*tanh((x-coeff(2))/coeff(3))+coeff(4);

% curve fitting
coeff0=     [1 1 1 1];
options = optimset('Algorithm','levenberg-marquardt','Display','off');                        % suppresses the warning
[coeff,~,~,~,output]=   lsqcurvefit(MODEL,coeff0,xSeg,ySeg_minmax,[],[],options);

% MODEL=   @(coeff,timeSeg) coeff(1)*(timeSeg-coeff(2)) + coeff(4);
% figure;hold on;plot(xSeg+x(yMinLoc),MODEL(coeff,xSeg))%;plot(xSeg+x(yMinLoc),ModelLIN(coeff,xSeg)); ylim([-2 2])
% figure; plot(xSeg+x(yMinLoc),MODEL(coeff,xSeg),'--')

coeff(2) = coeff(2) + x(yMinLoc);
coeff(4) = coeff(4) + ySeg_Mean;
% disp(['Amp: ',num2str(2*coeff(1)), ...
%     '   Shift: ',num2str(coeff(2)), ...
%     '   Anstieg: ',num2str(coeff(3)), ...
%     '   Offset: ',num2str(coeff(4))])

end